module bitbucket.org/vglab/pbxwatch

go 1.16

require (
	bitbucket.org/vglab/fsesl v0.0.0-20210722112019-569985e7ad91
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
