package main

import (
	api "bitbucket.org/vglab/fsesl/api"
)


var accounts = make(map[string]*account)
var uuids    = make(map[string]*channel)

func main() {
	apiC := api.NewClient()
	apiC.Connect("", 0, "")
	for {
		select {
		case apiE := <-apiC.Events:
			uuid := apiE.UUID
			if _, ok := uuids[uuid]; !ok {
				newChan := &channel{channelID: apiE.UUID}
				newChan.update(apiE)
				uuids[uuid] = newChan
			}
			chn := uuids[uuid]
			if chn.callp == nil {
				if apiE.AccountID != "" {
					if apiE.TrackID != "" {
						account := &account{accountID: apiE.AccountID, calls: map[string]*call{}}
						call    := &call{accountp: account, trackID: apiE.TrackID, channels: map[string]*channel{}}
						accounts[apiE.AccountID] = account
						account.calls[apiE.TrackID] = call
						call.channels[uuid] = chn
					}
				}
			}
			chn.update(apiE)
		}
	}
}

func (c *channel) update(apiE *api.Event) {

}
